package com.mycompany.myapp.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Record.
 */
@Entity
@Table(name = "T_RECORD")

public class Record implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;

    @Column(name = "json_request")
    private String jsonRequest;

    @Column(name = "xml_request")
    private String xmlRequest;

    @Column(name = "xml_response")
    private String xmlResponse;

    @Column(name = "service_type")
    private int serviceType;

    @Column(name = "user_login")
    private String userLogin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJsonRequest() {
        return jsonRequest;
    }

    public void setJsonRequest(String jsonRequest) {
        this.jsonRequest = jsonRequest;
    }

    public String getXmlRequest() {
        return xmlRequest;
    }

    public void setXmlRequest(String xmlRequest) {
        this.xmlRequest = xmlRequest;
    }

    public String getXmlResponse() {
        return xmlResponse;
    }

    public void setXmlResponse(String xmlResponse) {
        this.xmlResponse = xmlResponse;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int type) {
        this.serviceType = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Record record = (Record) o;

        if (id != record.id) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"serviceType\":" + serviceType +
                "}";
    }
}