package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Record;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Record entity.
 */
public interface RecordRepository extends JpaRepository<Record, Long> {
    List<Record> findByServiceType(Integer serviceType);
}
