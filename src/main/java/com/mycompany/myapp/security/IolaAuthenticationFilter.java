package com.mycompany.myapp.security;

import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.wscalls.utils.IOLAUtils;
import com.sun.org.apache.xpath.internal.XPathAPI;
import kz.iola.asn1.DERObject;
import kz.iola.asn1.cms.ContentInfo;
import kz.iola.asn1.cms.SignedData;
import kz.iola.jce.provider.IolaProvider;
import kz.iola.jce.provider.cms.CMSException;
import kz.iola.ocsp.CertificateID;
import kz.iola.openssl.PEMReader;
import kz.iola.util.encoders.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IolaAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final Logger log = LoggerFactory.getLogger(IolaAuthenticationFilter.class);

    @Inject
    private UserRepository userRepository;

	private boolean postOnly = true;

    private String iosIIN = null;

	private static Pattern serialNumber = Pattern.compile("SERIALNUMBER=IIN([^=,]*)", Pattern.CASE_INSENSITIVE);

	public IolaAuthenticationFilter() {
		super("/app/iola_authentication");
	}

	@SuppressWarnings("restriction")
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
												HttpServletResponse response) throws AuthenticationException,
			IOException, ServletException {

		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

        String iinSaved = null;

		User user = null;
		String x509cert = null;
		if (request.getParameter("signature") != null) {
			x509cert = request.getParameter("signature");
			log.debug("x509cert = " + x509cert);
		} else {
			throw new AuthenticationServiceException("Something happened wrong!");
		}
		if (x509cert != null) {
			try {
                String x509original = x509cert;
				//preparing Xml: unescape
                //x509cert = x509cert.replaceAll("\\s+","");
				if (x509cert.indexOf("+") != -1) {
					x509cert = x509cert.replace("+", "%2b");
				}
				x509cert = URLDecoder.decode(x509cert, "UTF-8");

				// check security token
				String signed_data = (String) request.getSession().getAttribute("securitytoken_uuid");

                //StringUtils.substringBetween begin
                String signedDataFromXml = null;
                String open = "<extra>";
                String close = "</extra>";
                int start = x509cert.indexOf(open);
                if (start != -1) {
                    int end = x509cert.indexOf(close, start + open.length());
                    if (end != -1) {
                        signedDataFromXml = x509cert.substring(start + open.length(), end);
                    }
                }
                //StringUtils.substringBetween end

				log.info(">>>>>>>>>> sign data1 : " + signed_data);
				log.info(">>>>>>>>>> sign data2 : " + signedDataFromXml);
				/*if (!signed_data.equals(signedDataFromXml)) {
					throw new AuthenticationServiceException("we have got the wrong token!");
				}*/
				//

				Security.addProvider(new IolaProvider());
				org.apache.xml.security.Init.init();
				//session.setAttribute("android", "true");
				String isAndroid = (String) request.getSession().getAttribute("android");

				String checkXMLStatus = null;
				//checkXMLStatus = IOLAUtils.checkXML(x509cert);
                checkXMLStatus = "OK";
				log.debug("checkXMLStatus = " + checkXMLStatus);
				if (checkXMLStatus.equals("OK")) {
					X509Certificate certKey = null;
                    certKey = getIOSCertificate(x509original);

					if (certKey != null) {
						Date notAfter = certKey.getNotAfter();
						Date notBefore = certKey.getNotBefore();
						Date now = new Date();
						System.out.println("CHECK DATE => " + notBefore.toString() + ":::" + notAfter.toString() + ":::" + now.toString());

						if (!(notBefore.before(now) && now.before(notAfter))) {
							throw new AuthenticationServiceException("Certificate date expired");
						}

						String name = certKey.getSubjectDN().getName();
						Matcher m = serialNumber.matcher(name);
						if (!m.find())
							throw new BadCredentialsException("There's no serialNumber field in provided certificate.");
						log.debug(m.toString() + "!!!! m.find=" + m.group(1));
                        iinSaved = m.group(1);
                        user = userRepository.findOne(m.group(1));
						//user = userDao.findByIin(m.group(1));
					} else if (iosIIN != null) {
                        user = userRepository.findOne(iosIIN);
                        System.out.print("IOS IIN  = " + iosIIN);
                    }
				}
			} catch (Exception e) {
				log.error("", e);
			}
		} else {
			throw new AuthenticationServiceException("Something happened wrong!");
		}
		//user = userDao.findByIin("830608302758");
		if (user != null) {
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(user.getLogin(), "user");
			// Allow subclasses to set the "details" property
			authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
			//setDetails(request, authRequest);
			return this.getAuthenticationManager().authenticate(authRequest);
		} else {
			throw new AuthenticationServiceException("No user with such IIN in system!");
		}
	}

    private X509Certificate getIOSCertificate(String x509cert) {
        System.out.println("getIOSCertificate");
        try {
			/*
			 * CertificateFactory cf = CertificateFactory.getInstance("X.509");
			 *
			 * // Get ContentInfo //byte[] signature = ... // PKCS#7 signature bytes InputStream signatureIn = new
			 * ByteArrayInputStream(x509cert.getBytes()); DERObject obj = new
			 * ASN1InputStream(signatureIn).readObject(); ContentInfo contentInfo = ContentInfo.getInstance(obj);
			 *
			 * // Extract certificates SignedData signedData = SignedData.getInstance(contentInfo.getContent());
			 * System.out.println(signedData); Enumeration certificates =
			 * signedData.getCertificates().getObjects(); System.out.println(certificates); // Build certificate path
			 * List certList = new ArrayList(); while (certificates.hasMoreElements()) {
			 * DERObject certObj = (DERObject) certificates.nextElement(); InputStream in = new ByteArrayInputStream
			 * (certObj.getDEREncoded()); certList.add(cf.generateCertificate(in)); }
			 * System.out.println(certList.size()); System.out.println(certList); CertPath certPath = cf
			 * .generateCertPath(certList); System.out.println(certPath);
			 */
            verifyPkcs7("123", x509cert);
			/*InputStream signatureIn = new ByteArrayInputStream(x509cert.getBytes());
			PKCS7 pkcs7 = new PKCS7(signatureIn);

			System.out.println(pkcs7);
			// *** Checking some PKCS#7 parameters here

			X509Certificate prevCert = null; // Previous certificate we've found
			X509Certificate[] certs = pkcs7.getCertificates(); // `java.security.cert.X509Certificate`
			System.out.println(certs);
			for (int i = 0; i < certs.length; i++) {
				// *** Checking certificate validity period here
				System.out.println(certs[i]);
				if (certs[i] != null) {
					System.out.println(certs[i].getSubjectDN().getName());
					// Verify previous certificate in chain against this one
					prevCert.verify(certs[i].getPublicKey());
				}
				prevCert = certs[i];
			}*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public boolean verifyPkcs7(String dataToVerify, String signToVerify) throws CMSException, IOException,
        NoSuchAlgorithmException,
        NoSuchProviderException, CertStoreException, CertificateException {
        String x509certDecoded = URLDecoder.decode(signToVerify, "UTF-8");
        String x509certDecoded2 = signToVerify;
        if (x509certDecoded2.indexOf("+") != -1) {
            x509certDecoded2 = x509certDecoded2.replace("+", "%2b");
        }
        x509certDecoded2 = URLDecoder.decode(x509certDecoded2, "UTF-8");

        System.out.println(x509certDecoded);
        System.out.println(Base64.decode(x509certDecoded));
        System.out.println(" ====================== ");
        System.out.println(x509certDecoded2);
        System.out.println(Base64.decode(x509certDecoded2));
        System.out.println(" ====================== ");
        System.out.println(signToVerify);
        System.out.println(Base64.decode(signToVerify));
        System.out.println(" ====================== ");

        FileOutputStream output = new FileOutputStream(new File("signiostest1"));
        IOUtils.write(Base64.decode(x509certDecoded), output);

        FileOutputStream output2 = new FileOutputStream(new File("signiostest2"));
        IOUtils.write(Base64.decode(x509certDecoded2), output2);

        FileOutputStream output3 = new FileOutputStream(new File("signiostest3"));
        IOUtils.write(Base64.decode(signToVerify), output3);

        InputStream signatureIn = new ByteArrayInputStream(Base64.decode(x509certDecoded));
        PEMReader pemParser = new PEMReader(new InputStreamReader(signatureIn));

        ContentInfo object = (ContentInfo) pemParser.readObject();

        System.out.println(" =========== 888 =========== ");
        System.out.println(object);

        System.out.println(" =========== 888 pub =========== ");
        //System.out.println(object.getContent());
        SignedData signedData = SignedData.getInstance(object.getContent());
        Enumeration certificates = signedData.getCertificates().getObjects();
        Enumeration e2 = signedData.getSignerInfos().getObjects();
        //Enumeration e3 = signedData.getCRLs().getObjects();
        Enumeration e4 = signedData.getDigestAlgorithms().getObjects();
        while (e2.hasMoreElements()) {
            System.out.println(" ===========  0008878  =========== ");
            DERObject certObj = (DERObject) e2.nextElement();
            System.out.println(certObj);
        }
        while (e4.hasMoreElements()) {
            System.out.println(" ===========  0008878333 4444  =========== ");
            DERObject certObj = (DERObject) e4.nextElement();
            System.out.println(certObj);
        }
        //System.out.println(certificates);
        List certList = new ArrayList();
        //CertificateFactory cf = CertificateFactory.getInstance("X.509");
        while (certificates.hasMoreElements()) {
            DERObject certObj = (DERObject) certificates.nextElement();
            System.out.println(certObj.toASN1Object());
            InputStream in = new ByteArrayInputStream(certObj.getDEREncoded());

            String certObjStr = certObj.toString();
            int beginIndex = certObjStr.indexOf("IIN");
            iosIIN = certObjStr.substring(beginIndex + 3, beginIndex + 15);
            //ASN1Sequence sequence=(ASN1Sequence)certObj;
            //SignerInfo sf = new SignerInfo(sequence);
            //CMSSignedData signedData2 = new CMSSignedData(certObj.getEncoded());
            //certList.add(cf.generateCertificate(in));
            System.out.println(" ===========  00044  =========== ");
            System.out.println(iosIIN);
        }

        //Signature signature = Signature.getInstance("SHA1withRSA", "IOLA");

		/*KeyPair keyPair = (KeyPair) pemReader.readObject();

		signature.initVerify(publicKey);
	    signature.update(data);
	    System.out.println(signature.verify(signatureData));*/

        pemParser.close();

		/*PEMParser pemParser = new PEMParser(new BufferedReader(new FileReader("signiostest1")));
		Object object = pemParser.readObject();

		JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
		KeyPair kp = converter.getKeyPair((PEMKeyPair) object);
		PrivateKey privateKey = kp.getPrivate();

		System.out.println(" =========== 888 =========== ");
		System.out.println(privateKey.getAlgorithm());

		Signature sig = Signature.getInstance("RSA");*/
        //sig.initSign(privateKey);
        //sig.update(stringToSign.getBytes());

        //byte[] bytes = sig.sign();
        //System.out.println(new String(Base64.encode(bytes))); //+ close pemParser

        //
        //CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // Get ContentInfo
        //byte[] signature = ... // PKCS#7 signature bytes

        System.out.println(" ===========  0001  =========== ");

		/*Certificate cert = cf.generateCertificate(signatureIn);
		System.out.println(cert);


		DERObject obj = new ASN1InputStream(signatureIn).readObject();
		System.out.println(obj);
		ContentInfo contentInfo = ContentInfo.getInstance(obj);
		System.out.println(" ===========  0002  =========== ");
		System.out.println(contentInfo);
		// Extract certificates
		SignedData signedData = SignedData.getInstance(contentInfo.getContent());
		Enumeration certificates = signedData.getCertificates().getObjects();

		System.out.println(" ===========  0003  =========== ");
		System.out.println(certificates);
		// Build certificate path
		List certList = new ArrayList();
		while (certificates.hasMoreElements()) {
		    DERObject certObj = (DERObject) certificates.nextElement();
		    InputStream in = new ByteArrayInputStream(certObj.getDEREncoded());
		    certList.add(cf.generateCertificate(in));
		    System.out.println(" ===========  00044  =========== ");
		    System.out.println(cf.generateCertificate(in).getType());
		}
		CertPath certPath = cf.generateCertPath(certList);
		System.out.println(" ===========  0005  =========== ");
	    System.out.println(certPath);*/
        // Load key store
        //String keyStorePath = ...
        //KeyStore keyStore = KeyStore.getInstance("JKS");
        //keyStore.load(new FileInputStream(keyStorePath), null);

        // Set validation parameters
        //PKIXParameters params = new PKIXParameters(keyStore);
        //params.setRevocationEnabled(false); // to avoid exception on empty CRL

        // Validate certificate path
        //CertPathValidator validator = CertPathValidator.getInstance("PKIX");
        //CertPathValidatorResult result = validator.validate(certPath, params);




		/*Signature signature = Signature.getInstance("SHA1withRSA", "IOLA");
		signature.initVerify(publicKey);
	    signature.update(data);
	    return signature.verify(signatureData);*/

		/*CMSSignedData signedData = new CMSSignedData(Base64.decode(x509certDecoded));
		System.out.println(signedData);
		boolean isAttachedContent = signedData.getSignedContent() != null;
		if (isAttachedContent) {
			signedData = new CMSSignedData(signedData.getEncoded());
		} else {
			CMSProcessableByteArray data = new CMSProcessableByteArray(dataToVerify.getBytes("UTF-8"));
			signedData = new CMSSignedData(data, signedData.getEncoded());
		}
		System.out.println(signedData);
		SignerInformationStore signers = signedData.getSignerInfos();
		CertStore certs = signedData.getCertificatesAndCRLs("Collection", "IOLA");
		Iterator<?> it = signers.getSigners().iterator();
		if (it.hasNext()) {
			SignerInformation signer = (SignerInformation) it.next();
			X509CertSelector signerConstraints = signer.getSID();
			signerConstraints.setKeyUsage(getKeyUsageForSignature());

			System.out.println(signer);
			System.out.println(signer.getEncryptionAlgOID());
			Collection<?> certCollection = certs.getCertificates(signerConstraints);
			Iterator<?> certIt = certCollection.iterator();

			boolean isSigningUsage = false;
			while (certIt.hasNext()) {
				isSigningUsage = true;
				X509Certificate cert = (X509Certificate) certIt.next();
				System.out.println(cert);
				if (signer.verify(cert, "IOLA")) {
					return true;
				}
			}
			if (!isSigningUsage) {
				System.out.println("One or more certificates within PKCS7 cetificate chain have no non-repudiation and
				 digital signature key usages");
			}
		}*/
        return false;
    }

	private X509Certificate getAndroidCertificate(String x509cert) throws SAXException, ParserConfigurationException,
			IOException, TransformerException, XMLSignatureException, XMLSecurityException {
		Document doc = IOLAUtils.createXmlDocumentFromString(x509cert, "UTF-8");
		Element nscontext = XMLUtils.createDSctx(doc, "ds", "http://www.w3.org/2000/09/xmldsig#");
		Element signelement = (Element) XPathAPI.selectSingleNode(doc, "//ds:Signature", nscontext);
		XMLSignature signature = new XMLSignature(signelement, "");
		KeyInfo ki = signature.getKeyInfo();
		X509Certificate certKey = ki.getX509Certificate();
		return certKey;
	}

	protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}
}
