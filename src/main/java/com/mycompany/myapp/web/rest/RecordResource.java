package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mycompany.myapp.domain.Record;
import com.mycompany.myapp.repository.RecordRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.wscalls.IpnWebServiceCall;
import com.mycompany.myapp.wscalls.OilWebServiceCall;
import com.mycompany.myapp.wscalls.TestWebServiceCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * REST controller for managing Service.
 */
@RestController
@RequestMapping("/app")
public class RecordResource {
    private final Logger log = LoggerFactory.getLogger(RecordResource.class);

    @Inject
    private RecordRepository recordRepository;

    /**
     * POST  /rest/service1 -> Обрабатываем запрос по первому сервису.
     * "Транзитные таможенные декларации (Таможня)"
     */
    @RequestMapping(value = "/rest/service1",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public Object service1(@RequestBody Map<String, String> requestMap) {
        log.debug("REST request to get Service1 : {}");

        TestWebServiceCall serviceCall = new TestWebServiceCall();
        Map<String, Object> wsResult = new HashMap<>();

        String searchType = requestMap.get("searchType");
        if (searchType.equals("1")) {
            String iinBin = requestMap.get("iinBin");
            String rnn = requestMap.get("rnn");
            // call WebService here
            wsResult = serviceCall.findPersonByIin(iinBin, rnn);
        } else if (searchType.equals("2")) {
            String surname = requestMap.get("surname");
            String firstName = requestMap.get("firstName");
            String middleName = requestMap.get("middleName");
            String birthDateStr = requestMap.get("birthDate");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = null;
            try {
                birthDate = format.parse(birthDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // call WebService here
            wsResult = serviceCall.findPersonByFio(surname, firstName, middleName, birthDate);
        } else if (searchType.equals("3")) {
            String companyName = requestMap.get("companyName");
            // call WebService here
            wsResult = serviceCall.findPersonByCompanyName(companyName);
        }

        if (wsResult.get("RESULT") == null)
            return "false";
        else {
            Record r = new Record();
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = "";
            try {
                json = ow.writeValueAsString(wsResult.get("RESULT"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            r.setJsonRequest(json);
            r.setXmlRequest(wsResult.get("OUT").toString());
            r.setXmlResponse(wsResult.get("IN").toString());
            r.setServiceType(1);
            r.setUserLogin(SecurityUtils.getCurrentLogin());
            recordRepository.save(r);

            return r.toString();
        }
    }

    @RequestMapping(value = "/rest/service1/view",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public Record get1(@RequestBody long id) {
        log.debug("REST request to get one service 1 record : {}", id);
        Record r = recordRepository.findOne(id);
        //if (f == null) {
        //response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        //}

        return r;
    }

    /**
     * POST  /rest/service2 -> Обрабатываем запрос по второму сервису.
     * "Сведения о плательщиках НДС (НК)"
     */
    @RequestMapping(value = "/rest/service2",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public String service2(@RequestBody Map<String, String> requestMap) {
        log.debug("REST request to get Service2 : {}");

        IpnWebServiceCall serviceCall = new IpnWebServiceCall();
        Map<String, Object> wsResult = new HashMap<>();

        String searchType = requestMap.get("searchType");
        if (searchType.equals("1")) {
            String iinBin = requestMap.get("iinBin");
            String rnn = requestMap.get("rnn");
            // call WebService here
            wsResult = serviceCall.findPersonByIin(iinBin, rnn);
        } else if (searchType.equals("2")) {
            String surname = requestMap.get("surname");
            String firstName = requestMap.get("firstName");
            String middleName = requestMap.get("middleName");
            // call WebService here
            wsResult = serviceCall.findPersonByFio(surname, firstName, middleName);
        }

        if (wsResult.get("RESULT") == null)
            return "false";
        else {
            Record r = new Record();
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = "";
            try {
                json = ow.writeValueAsString(wsResult.get("RESULT"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            r.setJsonRequest(json);
            r.setXmlRequest(wsResult.get("OUT").toString());
            r.setXmlResponse(wsResult.get("IN").toString());
            r.setServiceType(2);
            r.setUserLogin(SecurityUtils.getCurrentLogin());
            recordRepository.save(r);

            return r.toString();
        }
    }

    @RequestMapping(value = "/rest/service2/view",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public Record get2(@RequestBody long id) {
        log.debug("REST request to get one service 2 record : {}", id);
        Record r = recordRepository.findOne(id);
        //if (f == null) {
        //response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        //}

        return r;
    }

    /**
     * POST  /rest/service3 -> Обрабатываем запрос по третьему сервису.
     * "Сведения о СОНО (НК)"
     */
    @RequestMapping(value = "/rest/service3",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public String service3(@RequestBody Map<String, String> requestMap) {
        log.debug("REST request to get Service3 : {}");

        OilWebServiceCall serviceCall = new OilWebServiceCall();
        Map<String, Object> wsResult = new HashMap<>();

        String searchType = requestMap.get("searchType");
        if (searchType.equals("1")) {
            String iinBin = requestMap.get("iinBin");
            String rnn = requestMap.get("rnn");
            // call WebService here
            wsResult = serviceCall.findPersonByIin(iinBin, rnn);
        } else if (searchType.equals("2")) {
            String surname = requestMap.get("surname");
            String firstName = requestMap.get("firstName");
            String middleName = requestMap.get("middleName");
            String birthDateStr = requestMap.get("birthDate");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = null;
            try {
                birthDate = format.parse(birthDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // call WebService here
            wsResult = serviceCall.findPersonByFio(surname, firstName, middleName, birthDate);
        } else if (searchType.equals("3")) {
            String companyName = requestMap.get("companyName");
            // call WebService here
            wsResult = serviceCall.findPersonByCompanyName(companyName);
        }

        if (wsResult.get("RESULT") == null)
            return "false";
        else {
            Record r = new Record();
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = "";
            try {
                json = ow.writeValueAsString(wsResult.get("RESULT"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            r.setJsonRequest(json);
            r.setXmlRequest(wsResult.get("OUT").toString());
            r.setXmlResponse(wsResult.get("IN").toString());
            r.setServiceType(3);
            r.setUserLogin(SecurityUtils.getCurrentLogin());
            recordRepository.save(r);

            return r.toString();
        }
    }

    @RequestMapping(value = "/rest/service3/view",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public Record get3(@RequestBody long id) {
        log.debug("REST request to get one service 3 record : {}", id);
        Record r = recordRepository.findOne(id);
        //if (f == null) {
        //response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        //}

        return r;
    }

    /**
     * POST  /rest/service -> get all records.
     */
    @RequestMapping(value = "/rest/service/all",
        method = RequestMethod.POST,
        produces = "application/json")
    @Timed
    public List<Record> getAll(@RequestBody int id) {
        log.debug("REST request to get all records from service");

        return recordRepository.findByServiceType(id);
        //return recordRepository.findAll();
    }
}
