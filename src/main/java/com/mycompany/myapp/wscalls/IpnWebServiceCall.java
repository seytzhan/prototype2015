package com.mycompany.myapp.wscalls;

import com.mycompany.myapp.wscalls.handlers.MessageToStringHandler;
import com.mycompany.myapp.wscalls.handlers.SoftkeySignatureHandler;
import kz.ipn.service.services.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import java.net.URL;
import java.util.*;

public class IpnWebServiceCall {

    private IPNService service = null;

    public IpnWebServiceCall() {

        URL urlService = IpnWebServiceCall.class.getClassLoader().getResource("ipnService.xml");

        IPNService_Service test_Service = new IPNService_Service(urlService);
        service = test_Service.getIPNServiceImplPort();

        BindingProvider binding = (BindingProvider) service;
        List<Handler> handlers = new LinkedList<Handler>();
        handlers.add(new SoftkeySignatureHandler(true, false));
        binding.getBinding().setHandlerChain(handlers);
    }

    public Map<String, Object> findPersonByIin(String iinBin, String rnn) {
        IpnIinRequest findType = new IpnIinRequest();
        FindByIin findRequest = new FindByIin();
        findRequest.setIinBin(iinBin);
        findRequest.setRnn(rnn);
        findType.setFindByIinRequest(findRequest);

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setVersion("1");
        requestInfo.setStartIndex(0l);
        requestInfo.setItemsPerPage(100);
        UUID uid = UUID.randomUUID();
        requestInfo.setRequestID(uid.toString());
        Date nowDate = new Date();
        GregorianCalendar gDate = new GregorianCalendar();
        gDate.setTime(nowDate);
        XMLGregorianCalendar xmlDate = null;
        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        requestInfo.setRequestDateTime(xmlDate);
        findType.setRequestInfo(requestInfo);

        BindingProvider binding = (BindingProvider) service;
        MessageToStringHandler h = new MessageToStringHandler();
        List<Handler> newHandlers = new LinkedList<Handler>();
        newHandlers.addAll(binding.getBinding().getHandlerChain());
        newHandlers.add(h);
        binding.getBinding().setHandlerChain(newHandlers);

        IpnResponse response = service.findPersonByIinResponse(findType);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("IN", h.getIn());
        map.put("OUT", h.getOut());
        map.put("RESPONSE", response);
        map.put("RESULT", response.getIPNList());
        return map;
    }

    public Map<String, Object> findPersonByFio(String surname, String name, String middleName) {
        IpnFioRequest findType = new IpnFioRequest();
        FindByFio findRequest = new FindByFio();
        findRequest.setSurname(surname);
        findRequest.setName(name);
        findRequest.setMiddleName(middleName);
        findType.setFindByFioRequest(findRequest);

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setVersion("1");
        requestInfo.setStartIndex(0l);
        requestInfo.setItemsPerPage(100);
        UUID uid = UUID.randomUUID();
        requestInfo.setRequestID(uid.toString());
        Date nowDate = new Date();
        GregorianCalendar gDate = new GregorianCalendar();
        gDate.setTime(nowDate);
        XMLGregorianCalendar xmlDate = null;
        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        requestInfo.setRequestDateTime(xmlDate);
        findType.setRequestInfo(requestInfo);

        BindingProvider binding = (BindingProvider) service;
        MessageToStringHandler h = new MessageToStringHandler();
        List<Handler> newHandlers = new LinkedList<Handler>();
        newHandlers.addAll(binding.getBinding().getHandlerChain());
        newHandlers.add(h);
        binding.getBinding().setHandlerChain(newHandlers);

        IpnResponse response = service.findPersonByFioResponse(findType);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("IN", h.getIn());
        map.put("OUT", h.getOut());
        map.put("RESPONSE", response);
        map.put("RESULT", response.getIPNList());
        return map;
    }

}
