package com.mycompany.myapp.wscalls;

import com.mycompany.myapp.wscalls.handlers.MessageToStringHandler;
import com.mycompany.myapp.wscalls.handlers.SoftkeySignatureHandler;
import kz.oil.service.services.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import java.net.URL;
import java.util.*;

public class OilWebServiceCall {

    private OilService service = null;

    public OilWebServiceCall() {

        URL urlService = OilWebServiceCall.class.getClassLoader().getResource("oilService.xml");

        OilService_Service test_Service = new OilService_Service(urlService);
        service = test_Service.getOilServiceImplPort();

        BindingProvider binding = (BindingProvider) service;
        List<Handler> handlers = new LinkedList<Handler>();
        handlers.add(new SoftkeySignatureHandler(true, false));
        binding.getBinding().setHandlerChain(handlers);
    }

    public Map<String, Object> findPersonByIin(String iinBin, String rnn) {
        OilIinRequest findType = new OilIinRequest();
        FindByIin findRequest = new FindByIin();
        findRequest.setIinBin(iinBin);
        findRequest.setRnn(rnn);
        findType.setFindByIinRequest(findRequest);

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setVersion("1");
        requestInfo.setStartIndex(0l);
        requestInfo.setItemsPerPage(100);
        UUID uid = UUID.randomUUID();
        requestInfo.setRequestID(uid.toString());
        Date nowDate = new Date();
        GregorianCalendar gDate = new GregorianCalendar();
        gDate.setTime(nowDate);
        XMLGregorianCalendar xmlDate = null;
        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        requestInfo.setRequestDateTime(xmlDate);
        findType.setRequestInfo(requestInfo);

        BindingProvider binding = (BindingProvider) service;
        MessageToStringHandler h = new MessageToStringHandler();
        List<Handler> newHandlers = new LinkedList<Handler>();
        newHandlers.addAll(binding.getBinding().getHandlerChain());
        newHandlers.add(h);
        binding.getBinding().setHandlerChain(newHandlers);

        OilResponse response = service.findPersonByIinResponse(findType);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("IN", h.getIn());
        map.put("OUT", h.getOut());
        map.put("RESPONSE", response.getResponseInfo());
        map.put("RESULT", response.getOilList());
        return map;
    }

    public Map<String, Object> findPersonByFio(String surname, String name, String middleName, Date birthDate) {
        OilFioRequest findType = new OilFioRequest();
        FindByFio findRequest = new FindByFio();
        findRequest.setSurname(surname);
        findRequest.setName(name);
        findRequest.setMiddleName(middleName);
        GregorianCalendar birthGDate = new GregorianCalendar();
        birthGDate.setTime(birthDate);
        XMLGregorianCalendar birthXmlDateDate = null;
        try {
            birthXmlDateDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(birthGDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        findRequest.setBirthDate(birthXmlDateDate);
        findType.setFindByFioRequest(findRequest);

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setVersion("1");
        requestInfo.setStartIndex(0l);
        requestInfo.setItemsPerPage(100);
        UUID uid = UUID.randomUUID();
        requestInfo.setRequestID(uid.toString());
        Date nowDate = new Date();
        GregorianCalendar gDate = new GregorianCalendar();
        gDate.setTime(nowDate);
        XMLGregorianCalendar xmlDate = null;
        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        requestInfo.setRequestDateTime(xmlDate);
        findType.setRequestInfo(requestInfo);

        BindingProvider binding = (BindingProvider) service;
        MessageToStringHandler h = new MessageToStringHandler();
        List<Handler> newHandlers = new LinkedList<Handler>();
        newHandlers.addAll(binding.getBinding().getHandlerChain());
        newHandlers.add(h);
        binding.getBinding().setHandlerChain(newHandlers);

        OilResponse response = service.findPersonByFioResponse(findType);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("IN", h.getIn());
        map.put("OUT", h.getOut());
        map.put("RESPONSE", response.getResponseInfo());
        map.put("RESULT", response.getOilList());
        return map;
    }

    public Map<String, Object> findPersonByCompanyName(String companyName) {
        OilULRequest findType = new OilULRequest();
        FindByULName findRequest = new FindByULName();
        findRequest.setCompanyName(companyName);
        findType.setFindByULNameRequest(findRequest);

        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setVersion("1");
        requestInfo.setStartIndex(0l);
        requestInfo.setItemsPerPage(100);
        UUID uid = UUID.randomUUID();
        requestInfo.setRequestID(uid.toString());
        Date nowDate = new Date();
        GregorianCalendar gDate = new GregorianCalendar();
        gDate.setTime(nowDate);
        XMLGregorianCalendar xmlDate = null;
        try {
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gDate);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        requestInfo.setRequestDateTime(xmlDate);
        findType.setRequestInfo(requestInfo);

        BindingProvider binding = (BindingProvider) service;
        MessageToStringHandler h = new MessageToStringHandler();
        List<Handler> newHandlers = new LinkedList<Handler>();
        newHandlers.addAll(binding.getBinding().getHandlerChain());
        newHandlers.add(h);
        binding.getBinding().setHandlerChain(newHandlers);

        OilResponse response = service.findPersonByCompanyNameResponse(findType);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("IN", h.getIn());
        map.put("OUT", h.getOut());
        map.put("RESPONSE", response.getResponseInfo());
        map.put("RESULT", response.getOilList());
        return map;
    }
}
