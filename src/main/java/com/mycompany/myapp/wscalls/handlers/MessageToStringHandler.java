package com.mycompany.myapp.wscalls.handlers;

import org.apache.log4j.Logger;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.StringWriter;
import java.util.Set;

public class MessageToStringHandler implements SOAPHandler<SOAPMessageContext> {

    private static Logger log = Logger.getLogger(MessageToStringHandler.class);

    private String message;
    private String in;
    private String out;

    public String getIn() {
        return in;
    }

    public String getOut() {
        return out;
    }

    public String getMessage() {
        return message;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Set getHeaders() {
        return null;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        SOAPMessage msg = context.getMessage();
        if (msg != null) {
            try {
                StringWriter os = new StringWriter();
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer trans = tf.newTransformer();
                trans.setOutputProperty(OutputKeys.INDENT, "yes");
                trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
                trans.transform(new DOMSource(msg.getSOAPPart()), new StreamResult(os));
                os.close();
                if (outbound) {
                    out = os.toString();
                    log.debug("OUT => " + out);
                } else {
                    in = os.toString();
                    message = os.toString();
                    log.debug("IN => " + in);
                }
            } catch (Exception e) {
                log.error("", e);
                return false;
            }
        }
        return true;
    }

}
