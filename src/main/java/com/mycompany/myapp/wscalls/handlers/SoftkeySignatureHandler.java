package com.mycompany.myapp.wscalls.handlers;

import com.mycompany.myapp.wscalls.utils.IOLAUtils;
import kz.iola.jce.provider.IolaProvider;
import kz.iola.xmldsig.JCPXMLDSigInit;
import org.apache.log4j.Logger;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Set;
import java.util.UUID;

public class SoftkeySignatureHandler implements SOAPHandler<SOAPMessageContext> {

    private static Logger log = Logger.getLogger(SoftkeySignatureHandler.class);
    private boolean sign = true;
    private boolean check = false;

    public SoftkeySignatureHandler() {
        this(true, true);
    }

    public SoftkeySignatureHandler(boolean sign, boolean check) {
        this.sign = sign;
        this.check = check;
        Provider prov = new IolaProvider();
        Security.addProvider(prov);
        org.apache.xml.security.Init.init();
        JCPXMLDSigInit.Init();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Set getHeaders() {
        return null;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        log.warn("FAULT");
        return true;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        boolean result = false;

        SOAPMessage msg = context.getMessage();

        String id = UUID.randomUUID().toString();

        if (outbound) {
            if (sign) {
                try {
                    SOAPEnvelope env = msg.getSOAPPart().getEnvelope();

                    SOAPHeader header = env.getHeader();
                    if (header == null) {
                        header = env.addHeader();
                    }

                    SOAPBody body = env.getBody();

                    body.addAttribute(new QName("Id"), id);

                    X509Certificate cer = IOLAUtils.getCertificate();
                    PrivateKey pkey = IOLAUtils.getPrivateKey();

                    XMLSignature signature = new XMLSignature(env.getOwnerDocument(), "", "http://www.w3.org/2001/04/xmldsig-more#gost34310-gost34311");
                    Transforms transforms = new Transforms(env.getOwnerDocument());
                    transforms.addTransform(Transforms.TRANSFORM_C14N_WITH_COMMENTS);
                    transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
                    signature.addDocument("#" + id, transforms, "http://www.w3.org/2001/04/xmldsig-more#gost34311");
                    signature.addKeyInfo(cer);

                    header.appendChild(signature.getElement());

                    signature.sign(pkey);

                    result = true;
                } catch (SOAPException e) {
                    log.error("", e);
                } catch (XMLSecurityException e) {
                    log.error("", e);
                } catch (Throwable e) {
                    log.error("", e);
                }
            }
//			try {
//				dumpMessage(msg,"out_"+id+".xml");
//			} catch (SOAPException e) {
//				log.error("", e);
//			} catch (IOException e) {
//				log.error("", e);
//			} catch (Exception e) {
//				log.error("", e);
//			}
        } else {
            if (check) {
                log.debug("=============> STARTING 0");
                try {
                    log.debug("=============> STARTING 1");
                    SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
                    log.debug("=============> STARTING 2");
                    Document doc = env.getOwnerDocument();
                    log.debug("=============> STARTING 3");
                    Element signatureElement = getSignature(doc);
                    log.debug("=============> STARTING 4");
                    XMLSignature signature = new XMLSignature(signatureElement, "");
                    log.debug("=============> STARTING 5");
                    X509Certificate certificate = signature.getKeyInfo().getX509Certificate();
                    log.debug("=============> CERTIFICATE SERIAL IS " + certificate.getSerialNumber());
                    result = signature.checkSignatureValue(certificate);
                    log.debug("=============> CHECK RESULT IS " + result);
                } catch (XMLSecurityException e) {
                    log.debug("=============> STARTING 6");
                    log.error("", e);
                } catch (SOAPException e) {
                    log.debug("=============> STARTING 7");
                    log.error("", e);
                } catch (Throwable e) {
                    log.debug("=============> STARTING 8");
                    log.error("", e);
                }
                log.debug("=============> STARTING 9");
            } else {
                log.debug("=============> STARTING 10");
                result = true;
            }
//			try {
//				dumpMessage(msg,"in_"+id+".xml");
//			} catch (SOAPException e) {
//				log.error("", e);
//			} catch (IOException e) {
//				log.error("", e);
//			} catch (Throwable e) {
//				log.error("", e);
//			}
        }
        return result;
    }

    private Element getSignature(Node parent) {
        Element signElement = null;
        if (parent.getNodeName().compareTo("ds:Signature") == 0) {
            signElement = (Element) parent;
        } else {
            for (int i = 0; i < parent.getChildNodes().getLength(); i++) {
                Node child = parent.getChildNodes().item(i);
                signElement = getSignature(child);
                if (signElement != null) break;
            }
        }
        return signElement;
    }

    private void dumpMessage(SOAPMessage msg, String filename) throws SOAPException, IOException {
        OutputStream out = new FileOutputStream(filename);
        msg.writeTo(out);
        out.close();
    }

}
