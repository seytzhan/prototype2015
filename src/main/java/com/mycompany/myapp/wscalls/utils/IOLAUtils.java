package com.mycompany.myapp.wscalls.utils;

import com.sun.org.apache.xpath.internal.XPathAPI;
import kz.iola.jce.provider.IolaProvider;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;

public class IOLAUtils {

    public static final String GOST_ALGORITHM_ECGOST34310 = "ECGOST34310";
    public static final String GOST_ALGORITHM_ECGOST3410 = "ECGOST3410";
    public static final String RSA_ALGORITHM_SHA1 = "SHA1";
    private static final String SIGN_METHOD_GOST = "http://www.w3.org/2001/04/xmldsig-more#gost34310-gost34311";
    private static final String DIGEST_METHOD_GOST = "http://www.w3.org/2001/04/xmldsig-more#gost34311";
    private static final String SIGN_METHOD_RSA = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha1";
    private static final String DIGEST_METHOD_RSA = "http://www.w3.org/2001/04/xmldsig-more#sha1";

    private static X509Certificate certificate = null;

    private static PrivateKey privateKey = null;

    static {
        try {
            certificate = loadCertificate();
            privateKey = loadPrivateKey();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
    }

    public static X509Certificate getCertificate() {
        return certificate;
    }

    public static PrivateKey getPrivateKey() {
        return privateKey;
    }

    public static void init() {
        Security.addProvider(new IolaProvider());
        org.apache.xml.security.Init.init();
    }

    private static X509Certificate loadCertificate() throws KeyStoreException, NoSuchProviderException, IOException,
            NoSuchAlgorithmException, CertificateException {
//		String p12file = Preferences.systemRoot().node("gp").get("keystore", null);
//		char[] p12password = Preferences.systemRoot().node("gp").get("password", "").toCharArray();
//		if (p12file == null) {
//			p12file = "/siopso/keys_gost.pfx";
//			p12password = "azat".toCharArray();
//		}
        String p12file = IOLAUtils.class.getResource("/GOSTKZ.p12").getPath();
        System.out.println("********************** 123 ********************");
        System.out.println("****************************************** " + p12file.toString());
        System.out.println("******************************************");
        char[] p12password="123456".toCharArray();

        byte[] p12bytes = FileUtils.readFileToByteArray(new File(p12file));

        KeyStore ks = KeyStore.getInstance("PKCS12", IolaProvider.PROVIDER_NAME);
        ks.load(new ByteArrayInputStream(p12bytes), p12password);

        @SuppressWarnings("rawtypes")
        Enumeration en = ks.aliases();
        String alias = null;
        while (en.hasMoreElements()) {
            alias = en.nextElement().toString();
        }
        X509Certificate certificate = (X509Certificate) ks.getCertificateChain(alias)[0];
        return certificate;
    }

    private static PrivateKey loadPrivateKey() throws IOException, KeyStoreException, NoSuchProviderException,
            NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException {
//		String p12file = Preferences.systemRoot().node("gp").get("keystore", null);
//		char[] p12password = Preferences.systemRoot().node("gp").get("password", "").toCharArray();
//		if (p12file == null) {
//			p12file = "/siopso/keys_gost.pfx";
//			p12password = "azat".toCharArray();
//		}

        String p12file = IOLAUtils.class.getResource("/GOSTKZ.p12").getPath();
        char[] p12password="123456".toCharArray();

        byte[] p12bytes = FileUtils.readFileToByteArray(new File(p12file));

        KeyStore ks = KeyStore.getInstance("PKCS12", IolaProvider.PROVIDER_NAME);
        ks.load(new ByteArrayInputStream(p12bytes), p12password);

        @SuppressWarnings("rawtypes")
        Enumeration en = ks.aliases();
        String alias = null;
        while (en.hasMoreElements()) {
            alias = en.nextElement().toString();
        }
        PrivateKey privateKey = (PrivateKey) ks.getKey(alias, p12password);
        return privateKey;
    }

    public static Document createXmlDocumentFromString(String xmlString, String charset) throws SAXException,
            ParserConfigurationException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
        Document doc = documentBuilder.parse(new ByteArrayInputStream(xmlString.getBytes(charset)));
        return doc;
    }

    public static Document signXmlDocument(Document document) throws UnrecoverableKeyException, KeyStoreException,
            NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException,
            TransformerConfigurationException, XMLSecurityException, TransformerException, SAXException,
            ParserConfigurationException, SOAPException {
        PrivateKey privateKey = loadPrivateKey();
        X509Certificate certificate = loadCertificate();
        Node xmlSignature = getXmlSignatureWithKeys(document, privateKey, certificate);

        document.getFirstChild().appendChild(xmlSignature);
        return document;
    }

    public static Document signSOAP(String xml) throws UnrecoverableKeyException, KeyStoreException,
            NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException, SOAPException {
        PrivateKey privateKey = loadPrivateKey();
        X509Certificate certificate = loadCertificate();

        InputStream in = IOUtils.toInputStream(xml);
        SOAPMessage message = MessageFactory.newInstance().createMessage(null, in);

        Document body = message.getSOAPBody().extractContentAsDocument();
        body.getFirstChild();
        return body;
    }

    public static String getXmlSignature(String xml) throws KeyStoreException, NoSuchProviderException, IOException,
            NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException,
            TransformerConfigurationException, XMLSecurityException, TransformerException, SAXException,
            ParserConfigurationException {
        return getXmlSignatureWithKeys(xml, privateKey, certificate);
    }

    public static Node getXmlSignatureWithKeys(Document doc, PrivateKey privateKey,
                                               X509Certificate certificate) throws XMLSecurityException,
            TransformerConfigurationException, IOException, TransformerException, SAXException,
            ParserConfigurationException {
        String signMethod = null;
        String digestMethod = null;

        if ((privateKey.getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST3410) == 0) || (privateKey
                .getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST34310) == 0)) {
            signMethod = SIGN_METHOD_GOST;
            digestMethod = DIGEST_METHOD_GOST;
        } else {
            signMethod = SIGN_METHOD_RSA;
            digestMethod = DIGEST_METHOD_RSA;
        }

        XMLSignature sig = new XMLSignature(doc, "", signMethod);
        Transforms transforms = new Transforms(doc);
        transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
        transforms.addTransform(Transforms.TRANSFORM_C14N11_OMIT_COMMENTS);
        sig.addDocument("#bodyId", transforms, digestMethod);
        sig.addKeyInfo((X509Certificate) certificate);
        sig.sign(privateKey);

        return sig.getElement();
    }

    public static XMLSignature getXmlSignature(Document doc, PrivateKey privateKey,
                                               X509Certificate certificate) throws XMLSecurityException,
            TransformerConfigurationException, IOException, TransformerException, SAXException,
            ParserConfigurationException {
        String signMethod = null;
        String digestMethod = null;

        if ((privateKey.getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST3410) == 0) || (privateKey
                .getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST34310) == 0)) {
            signMethod = SIGN_METHOD_GOST;
            digestMethod = DIGEST_METHOD_GOST;
        } else {
            signMethod = SIGN_METHOD_RSA;
            digestMethod = DIGEST_METHOD_RSA;
        }

        XMLSignature sig = new XMLSignature(doc, "", signMethod);
        Transforms transforms = new Transforms(doc);
        transforms.addTransform("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
        transforms.addTransform("http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments");
        sig.addDocument("#bodyId", transforms, digestMethod);
        sig.addKeyInfo((X509Certificate) certificate);
        sig.sign(privateKey);

        return sig;
    }

    public static String getXmlSignatureWithKeys(String xmlString, PrivateKey privateKey,
                                                 X509Certificate certificate) throws XMLSecurityException,
            TransformerConfigurationException, IOException, TransformerException, SAXException,
            ParserConfigurationException {
        String signMethod = null;
        String digestMethod = null;
        String signedXml = null;

        if ((privateKey.getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST3410) == 0) || (privateKey
                .getAlgorithm().toUpperCase().compareTo(GOST_ALGORITHM_ECGOST34310) == 0)) {
            signMethod = SIGN_METHOD_GOST;
            digestMethod = DIGEST_METHOD_GOST;
        } else {
            signMethod = SIGN_METHOD_RSA;
            digestMethod = DIGEST_METHOD_RSA;
        }

        Document doc = createXmlDocumentFromString(xmlString, "UTF-8");
        XMLSignature sig = new XMLSignature(doc, "", signMethod);

        if (doc.getFirstChild() != null) {
            doc.getFirstChild().appendChild(sig.getElement());
            Transforms transforms = new Transforms(doc);
            transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);
//			transforms.addTransform("http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments");
            sig.addDocument("#bodyId", transforms, digestMethod);
            sig.addKeyInfo((X509Certificate) certificate);
            sig.sign(privateKey);
            StringWriter os = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(doc), new StreamResult(os));
            os.flush();
            signedXml = os.toString();
            os.close();
        }

        int a = signedXml.indexOf("<ds:Signature");
        int b = signedXml.indexOf("</ds:Signature>");

        return signedXml.substring(a, b + 15);
    }

    public static String checkXML(String xml) throws SAXException, ParserConfigurationException, IOException,
            TransformerException, XMLSignatureException, XMLSecurityException {
        Document doc = createXmlDocumentFromString(xml, "UTF-8");
        Element nscontext = XMLUtils.createDSctx(doc, "ds", "http://www.w3.org/2000/09/xmldsig#");
        Element sigelement = (Element) XPathAPI.selectSingleNode(doc, "//ds:Signature", nscontext);
        if (sigelement == null) return "NO_SIGNATURE";

        XMLSignature signature = new XMLSignature(sigelement, "");
        KeyInfo ki = signature.getKeyInfo();
        X509Certificate certKey = ki.getX509Certificate();
        Date notAfter = certKey.getNotAfter();
        Date notBefore = certKey.getNotBefore();
        Date now = new Date();
        if (!(notBefore.before(now) && now.before(notAfter))) {
            return "CERT_DATE_INVALID";
        }

        boolean result = false;
        if (certKey != null) {
            System.out.println("CERTIFICATE IS NOT NULL");
            result = signature.getSignedInfo().verify();
//			result = signature.checkSignatureValue(certKey);
        }
        if (result) return "OK";
        else return "ERROR";
    }

    public static String toString(Document document) throws TransformerException {
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(document);
        trans.transform(source, result);
        String xmlString = sw.toString();
        return xmlString;
    }

}
