'use strict';

/* Controllers */

jhipsterApp.controller('MainController', ['$scope', '$location',
    function ($scope, $location) {
        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) == path) {
                return "active"
            } else {
                return ""
            }
        }
    }]);

jhipsterApp.controller('AdminController', ['$scope',
    function ($scope) {
    }]);

jhipsterApp.controller('LanguageController', ['$scope', '$translate',
    function ($scope, $translate) {
        $scope.changeLanguage = function (languageKey) {
            $translate.use(languageKey);
        };
    }]);

jhipsterApp.controller('MenuController', ['$scope',
    function ($scope) {
    }]);

jhipsterApp.controller('LoginController', ['$scope', '$rootScope', '$location', 'AuthenticationSharedService',
    function ($scope, $rootScope, $location, AuthenticationSharedService) {
        $scope.rememberMe = true;
        $scope.login = function () {
            AuthenticationSharedService.login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe,
                success: function () {
                    $location.path('');
                }
            })
        };
        $scope.loginByIola = function () {
            var signdata = "test";//$scope.signData;
            try {
                var selectedPassword = prompt('Введите пароль', '');
                window.JSInterface.setPassword(selectedPassword);

                var signed_xml = window.JSInterface.signXMLAuthentication(signdata);
                signed_xml = encodeURIComponent(signed_xml);

                AuthenticationSharedService.loginByIola({
                    signature: signed_xml,
                    success: function () {
                        $location.path('');
                    }
                });

            } catch (ex) {
                alert('error signing:' + ex);
            }

        };
        $scope.loginByAndroid = function () {
            var signdata = "test";//$scope.signData;
            try {
                var selectedPassword = prompt('Введите пароль', '');
                window.JSInterface.setPassword(selectedPassword);

                var signed_xml = window.JSInterface.signXMLAuthentication(signdata);
                signed_xml = encodeURIComponent(signed_xml);

                AuthenticationSharedService.loginByIola({
                    signature: signed_xml,
                    success: function () {
                        $location.path('');
                    }
                });

            } catch (ex) {
                alert('error signing:' + ex);
            }
        };
        $scope.loginByIOS = function () {
            var signdata = "test";//$scope.signData;
            try {
                var selectedPassword = prompt('Введите пароль', '');
                IOS_setPassword(selectedPassword);
                var signed_xml = IOS_signData(signdata);
                signed_xml = encodeURIComponent(signed_xml);
                AuthenticationSharedService.loginByIola({
                    signature: signed_xml,
                    success: function () {
                        $location.path('');
                    }
                });
            } catch (ex) {
                alert('error signing:' + ex);
            }
        };

    }]);

jhipsterApp.controller('LogoutController', ['$location', 'AuthenticationSharedService',
    function ($location, AuthenticationSharedService) {
        AuthenticationSharedService.logout({
            success: function () {
                $location.path('');
            }
        });
    }]);

jhipsterApp.controller('SettingsController', ['$scope', 'Account',
    function ($scope, Account) {
        $scope.success = null;
        $scope.error = null;
        $scope.settingsAccount = Account.get();

        $scope.save = function () {
            Account.save($scope.settingsAccount,
                function (value, responseHeaders) {
                    $scope.error = null;
                    $scope.success = 'OK';
                    $scope.settingsAccount = Account.get();
                },
                function (httpResponse) {
                    $scope.success = null;
                    $scope.error = "ERROR";
                });
        };
    }]);

jhipsterApp.controller('PasswordController', ['$scope', 'Password',
    function ($scope, Password) {
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.changePassword = function () {
            if ($scope.password != $scope.confirmPassword) {
                $scope.doNotMatch = "ERROR";
            } else {
                $scope.doNotMatch = null;
                Password.save($scope.password,
                    function (value, responseHeaders) {
                        $scope.error = null;
                        $scope.success = 'OK';
                    },
                    function (httpResponse) {
                        $scope.success = null;
                        $scope.error = "ERROR";
                    });
            }
        };
    }]);

jhipsterApp.controller('SessionsController', ['$scope', 'resolvedSessions', 'Sessions',
    function ($scope, resolvedSessions, Sessions) {
        $scope.success = null;
        $scope.error = null;
        $scope.sessions = resolvedSessions;
        $scope.invalidate = function (series) {
            Sessions.delete({series: encodeURIComponent(series)},
                function (value, responseHeaders) {
                    $scope.error = null;
                    $scope.success = "OK";
                    $scope.sessions = Sessions.get();
                },
                function (httpResponse) {
                    $scope.success = null;
                    $scope.error = "ERROR";
                });
        };
    }]);

jhipsterApp.controller('MetricsController', ['$scope', 'MetricsService', 'HealthCheckService', 'ThreadDumpService',
    function ($scope, MetricsService, HealthCheckService, ThreadDumpService) {

        $scope.refresh = function () {
            HealthCheckService.check().then(function (data) {
                $scope.healthCheck = data;
            });

            $scope.metrics = MetricsService.get();

            $scope.metrics.$get({}, function (items) {

                $scope.servicesStats = {};
                $scope.cachesStats = {};
                angular.forEach(items.timers, function (value, key) {
                    if (key.indexOf("web.rest") != -1) {
                        $scope.servicesStats[key] = value;
                    }

                    if (key.indexOf("net.sf.ehcache.Cache") != -1) {
                        // remove gets or puts
                        var index = key.lastIndexOf(".");
                        var newKey = key.substr(0, index);

                        // Keep the name of the domain
                        index = newKey.lastIndexOf(".");
                        $scope.cachesStats[newKey] = {
                            'name': newKey.substr(index + 1),
                            'value': value
                        };
                    }
                });
            });
        };

        $scope.refresh();

        $scope.threadDump = function () {
            ThreadDumpService.dump().then(function (data) {
                $scope.threadDump = data;

                $scope.threadDumpRunnable = 0;
                $scope.threadDumpWaiting = 0;
                $scope.threadDumpTimedWaiting = 0;
                $scope.threadDumpBlocked = 0;

                angular.forEach(data, function (value, key) {
                    if (value.threadState == 'RUNNABLE') {
                        $scope.threadDumpRunnable += 1;
                    } else if (value.threadState == 'WAITING') {
                        $scope.threadDumpWaiting += 1;
                    } else if (value.threadState == 'TIMED_WAITING') {
                        $scope.threadDumpTimedWaiting += 1;
                    } else if (value.threadState == 'BLOCKED') {
                        $scope.threadDumpBlocked += 1;
                    }
                });

                $scope.threadDumpAll = $scope.threadDumpRunnable + $scope.threadDumpWaiting +
                    $scope.threadDumpTimedWaiting + $scope.threadDumpBlocked;

            });
        };

        $scope.getLabelClass = function (threadState) {
            if (threadState == 'RUNNABLE') {
                return "label-success";
            } else if (threadState == 'WAITING') {
                return "label-info";
            } else if (threadState == 'TIMED_WAITING') {
                return "label-warning";
            } else if (threadState == 'BLOCKED') {
                return "label-danger";
            }
        };
    }]);

jhipsterApp.controller('LogsController', ['$scope', 'resolvedLogs', 'LogsService',
    function ($scope, resolvedLogs, LogsService) {
        $scope.loggers = resolvedLogs;

        $scope.changeLevel = function (name, level) {
            LogsService.changeLevel({name: name, level: level}, function () {
                $scope.loggers = LogsService.findAll();
            });
        }
    }]);

jhipsterApp.controller('AuditsController', ['$scope', '$translate', '$filter', 'AuditsService',
    function ($scope, $translate, $filter, AuditsService) {
        $scope.onChangeDate = function () {
            AuditsService.findByDates($scope.fromDate, $scope.toDate).then(function (data) {
                $scope.audits = data;
            });
        };

        // Date picker configuration
        $scope.today = function () {
            // Today + 1 day - needed if the current day must be included
            var today = new Date();
            var tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1); // create new increased date

            $scope.toDate = $filter('date')(tomorrow, "yyyy-MM-dd");
        };

        $scope.previousMonth = function () {
            var fromDate = new Date();
            if (fromDate.getMonth() == 0) {
                fromDate = new Date(fromDate.getFullYear() - 1, 0, fromDate.getDate());
            } else {
                fromDate = new Date(fromDate.getFullYear(), fromDate.getMonth() - 1, fromDate.getDate());
            }

            $scope.fromDate = $filter('date')(fromDate, "yyyy-MM-dd");
        };

        $scope.today();
        $scope.previousMonth();

        AuditsService.findByDates($scope.fromDate, $scope.toDate).then(function (data) {
            $scope.audits = data;
        });
    }]);

jhipsterApp.controller('Service1Controller', ['$scope', 'SERVICE1', '$rootScope',
    function ($scope, SERVICE1, $rootScope) {
        $rootScope.rResult = "";

        $scope.send = function () {
            $rootScope.loading = true;
            var data = {
                searchType: $scope.searchType,
                iinBin: $scope.iinBin,
                rnn: $scope.rnn,
                surname: $scope.surname,
                firstName: $scope.firstName,
                middleName: $scope.middleName,
                birthDate: $scope.birthDate,
                companyName: $scope.companyName
            };
            SERVICE1.sendRequestInsert(data);
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.iinBin = '';
            $scope.rnn = '';
            $scope.surname = '';
            $scope.firstName = '';
            $scope.middleName = '';
            $scope.birthDate = '';
            $scope.companyName = '';
        };

        $scope.getAll = function (id) {
            SERVICE1.sendRequestGetAll(id);
        };
        $scope.getAll(1);
    }
]);

jhipsterApp.controller('Service2Controller', ['$scope', 'SERVICE2', '$rootScope',
    function ($scope, SERVICE2, $rootScope) {
        $rootScope.rResult = "";

        $scope.send = function () {
            $rootScope.loading = true;
            var data = {
                searchType: $scope.searchType,
                iinBin: $scope.iinBin,
                rnn: $scope.rnn,
                surname: $scope.surname,
                firstName: $scope.firstName,
                middleName: $scope.middleName,
                birthDate: $scope.birthDate,
                companyName: $scope.companyName
            };
            SERVICE2.sendRequestInsert(data);
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.iinBin = '';
            $scope.rnn = '';
            $scope.surname = '';
            $scope.firstName = '';
            $scope.middleName = '';
            $scope.birthDate = '';
            $scope.companyName = '';
        };

        $scope.getAll = function (id) {
            SERVICE2.sendRequestGetAll(id);
        };
        $scope.getAll(2);
    }
]);

jhipsterApp.controller('Service3Controller', ['$scope', 'SERVICE3', '$rootScope',
    function ($scope, SERVICE3, $rootScope) {
        $rootScope.rResult = "";

        $scope.send = function () {
            $rootScope.loading = true;
            var data = {
                searchType: $scope.searchType,
                iinBin: $scope.iinBin,
                rnn: $scope.rnn,
                surname: $scope.surname,
                firstName: $scope.firstName,
                middleName: $scope.middleName,
                birthDate: $scope.birthDate,
                companyName: $scope.companyName
            };
            SERVICE3.sendRequestInsert(data);
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.iinBin = '';
            $scope.rnn = '';
            $scope.surname = '';
            $scope.firstName = '';
            $scope.middleName = '';
            $scope.birthDate = '';
            $scope.companyName = '';
        };

        $scope.getAll = function (id) {
            SERVICE3.sendRequestGetAll(id);
        };
        $scope.getAll(3);
    }
]);

jhipsterApp.controller('Service4Controller', ['$scope', 'SERVICE4', '$rootScope',
    function ($scope, SERVICE4, $rootScope) {
        $rootScope.rResult = "";

        $scope.send = function () {

            $rootScope.loading = true;
            var data = "request=" + $scope.rnn;
            $scope.rnn = '';
            SERVICE4.sendRequestInsert(data);
        };

        $scope.getAll = function (id) {
            SERVICE4.sendRequestGetAll(id);
        };
        $scope.getAll(4);
    }
]);

/*jhipsterApp.controller('Service5Controller', ['$scope', 'SERVICE5',
 function ($scope, SERVICE5) {
 $scope.send = function() {
 var request = Array();
 request.push($scope.lastName);
 request.push($scope.name);
 request.push($scope.middleName);
 request.push($scope.birthDate);
 request.push($scope.birthPlace);
 request.push($scope.lastPlaceOfRegistration);
 request.push($scope.causesOfLossOfRegistration);
 request.push($scope.temporaryHousing);
 request.push($scope.chronicDiseases);
 request.push($scope.disability);
 request.push($scope.workPlace);
 request.push($scope.dateOfRegistration);
 request.push($scope.dateDeregistration);
 request.push($scope.closeFamily);

 var data = "request=" + JSON.stringify(request);
 SERVICE5.sendRequestInsert(data);
 };

 $scope.getAll = function(id) {
 SERVICE5.sendRequestGetAll(id);
 };
 $scope.getAll(5);
 }
 ]);

 jhipsterApp.controller('Service6Controller', ['$scope', 'SERVICE6',
 function ($scope, SERVICE6) {
 $scope.send = function() {
 var request = Array();
 request.push($scope.lastName);
 request.push($scope.name);
 request.push($scope.middleName);
 request.push($scope.birthDate);
 request.push($scope.inn);
 request.push($scope.birthPlace);
 request.push($scope.lastPlaceOfRegistration);
 request.push($scope.dateOfRegistration);
 request.push($scope.reasonForRegistration);
 request.push($scope.dateDeregistration);

 var data = "request=" + JSON.stringify(request);
 SERVICE6.sendRequestInsert(data);
 };

 $scope.getAll = function(id) {
 SERVICE6.sendRequestGetAll(id);
 };
 $scope.getAll(6);
 }
 ]);

 jhipsterApp.controller('Service7Controller', ['$scope', 'SERVICE7',
 function ($scope, SERVICE7) {
 $scope.send = function() {
 var request = Array();
 request.push($scope.ulName);
 request.push($scope.lastName);
 request.push($scope.name);
 request.push($scope.middleName);
 request.push($scope.inn);
 request.push($scope.contractNumber);
 request.push($scope.phaseContract);
 request.push($scope.supplierContract);
 request.push($scope.contractStartDate);
 request.push($scope.contractCompletionDate);
 request.push($scope.arrears);
 request.push($scope.numDaysDelay);
 request.push($scope.dateRelevanceOfInformation);

 var data = "request=" + JSON.stringify(request);
 SERVICE7.sendRequestInsert(data);
 };

 $scope.getAll = function(id) {
 SERVICE7.sendRequestGetAll(id);
 };
 $scope.getAll(7);
 }
 ]);*/

jhipsterApp.controller('Service8Controller', ['$scope', 'SERVICE8', '$rootScope',
    function ($scope, SERVICE8, $rootScope) {
        $rootScope.rResult = "";

        $scope.send = function () {

            $rootScope.loading = true;
            var data = "request=" + $scope.writOfExecution;
            $scope.writOfExecution = '';
            SERVICE8.sendRequestInsert(data);
        };

        $scope.getAll = function (id) {
            SERVICE8.sendRequestGetAll(id);
        };
        $scope.getAll(8);
    }
]);
