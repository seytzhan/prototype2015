'use strict';

/* Services */

jhipsterApp.factory('Account', ['$resource',
    function ($resource) {
        return $resource('app/rest/account', {}, {
        });
    }]);

jhipsterApp.factory('Password', ['$resource',
    function ($resource) {
        return $resource('app/rest/account/change_password', {}, {
        });
    }]);

jhipsterApp.factory('Sessions', ['$resource',
    function ($resource) {
        return $resource('app/rest/account/sessions/:series', {}, {
            'get': { method: 'GET', isArray: true}
        });
    }]);

jhipsterApp.factory('MetricsService', ['$resource',
    function ($resource) {
        return $resource('metrics/metrics', {}, {
            'get': { method: 'GET'}
        });
    }]);

jhipsterApp.factory('ThreadDumpService', ['$http',
    function ($http) {
        return {
            dump: function () {
                var promise = $http.get('dump').then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
    }]);

jhipsterApp.factory('HealthCheckService', ['$rootScope', '$http',
    function ($rootScope, $http) {
        return {
            check: function () {
                var promise = $http.get('health').then(function (response) {
                    return response.data;
                });
                return promise;
            }
        };
    }]);

jhipsterApp.factory('LogsService', ['$resource',
    function ($resource) {
        return $resource('app/rest/logs', {}, {
            'findAll': { method: 'GET', isArray: true},
            'changeLevel': { method: 'PUT'}
        });
    }]);

jhipsterApp.factory('AuditsService', ['$http',
    function ($http) {
        return {
            findAll: function () {
                var promise = $http.get('app/rest/audits/all').then(function (response) {
                    return response.data;
                });
                return promise;
            },
            findByDates: function (fromDate, toDate) {
                var promise = $http.get('app/rest/audits/byDates', {params: {fromDate: fromDate, toDate: toDate}}).then(function (response) {
                    return response.data;
                });
                return promise;
            }
        }
    }]);

jhipsterApp.factory('Session', ['$cookieStore',
    function ($cookieStore) {
        this.create = function (login, firstName, lastName, email, userRoles) {
            this.login = login;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.userRoles = userRoles;
        };
        this.destroy = function () {
            this.login = null;
            this.firstName = null;
            this.lastName = null;
            this.email = null;
            this.roles = null;
            $cookieStore.remove('account');
        };
        return this;
    }]);

jhipsterApp.constant('USER_ROLES', {
    all: '*',
    admin: 'ROLE_ADMIN',
    user: 'ROLE_USER'
});

jhipsterApp.factory('AuthenticationSharedService', ['$rootScope', '$http', '$cookieStore', 'authService', 'Session', 'Account',
    function ($rootScope, $http, $cookieStore, authService, Session, Account) {
        return {
            login: function (param) {
                var data = "j_username=" + param.username + "&j_password=" + param.password + "&_spring_security_remember_me=" + param.rememberMe + "&submit=Login";
                $http.post('app/authentication', data, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    Account.get(function (data) {
                        Session.create(data.login, data.firstName, data.lastName, data.email, data.roles);
                        $cookieStore.put('account', JSON.stringify(Session));
                        authService.loginConfirmed(data);
                    });
                }).error(function (data, status, headers, config) {
                    Session.destroy();
                });
            },
            loginByIola: function (param) {
                var data = "signature=" + param.signature + "&submit=LoginByIola";
                $http.post('app/iola_authentication', data, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    Account.get(function (data) {
                        Session.create(data.login, data.firstName, data.lastName, data.email, data.roles);
                        $cookieStore.put('account', JSON.stringify(Session));
                        authService.loginConfirmed(data);
                    });
                }).error(function (data, status, headers, config) {
                    Session.destroy();
                });
            },
            isAuthenticated: function () {
                if (!Session.login) {
                    // check if the user has a cookie
                    if ($cookieStore.get('account') != null) {
                        var account = JSON.parse($cookieStore.get('account'));
                        Session.create(account.login, account.firstName, account.lastName,
                            account.email, account.userRoles);
                        $rootScope.account = Session;
                    }
                }
                return !!Session.login;
            },
            isAuthorized: function (authorizedRoles) {
                if (!angular.isArray(authorizedRoles)) {
                    if (authorizedRoles == '*') {
                        return true;
                    }

                    authorizedRoles = [authorizedRoles];
                }

                var isAuthorized = false;

                angular.forEach(authorizedRoles, function (authorizedRole) {
                    var authorized = (!!Session.login &&
                        Session.userRoles.indexOf(authorizedRole) !== -1);

                    if (authorized || authorizedRole == '*') {
                        isAuthorized = true;
                    }
                });

                return isAuthorized;
            },
            logout: function () {
                $rootScope.authenticationError = false;
                $http.get('app/logout')
                    .success(function (data, status, headers, config) {
                        Session.destroy();
                        authService.loginCancelled();
                    });
            }
        };
    }]);

jhipsterApp.factory('SERVICE1', ['$rootScope', '$http', '$window',
    function ($rootScope, $http, $window) {
        return {
            sendRequestInsert: function (data) {
                $http.post('app/rest/service1', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (result, status, headers, config) {
                    $rootScope.loading = false;
                    if (result == "false") {
                        $rootScope.rResult = "Ничего не найдено";
                    } else {
                        $rootScope.response = result;
                        var td2 = document.createElement("td");
                        td2.innerHTML = result.id;
                        var td3 = document.createElement("td");
                        td3.innerHTML = "<a href=\"#/service1/view/" + result.id + "\">Посмотреть запрос</a>";
                        var tr = document.createElement("tr");
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        var tableBody = document.getElementById("response_list");
                        tableBody.appendChild(tr);
                    }
                }).error(function (data, status, headers, config) {
                    $rootScope.loading = false;
                    $rootScope.rResult = "Ошибка запроса";
                });
            },

            sendRequestGetAll: function (data) {

                $http.post('app/rest/service/all', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    data.forEach(function (entry) {
                        entry.jsonRequest = JSON.parse(entry.jsonRequest);
                        //console.log(entry.jsonRequest);
                        entry.jsonRequest = entry.jsonRequest.descriptionOfGoods;
                    });
                    $rootScope.answer = data;
                }).error(function (data, status, headers, config) {
                    //$window.alert("Ошибка запроса");
                });
            },

            sendRequestGet: function (data) {
                $http.post('app/rest/service1/view', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    data.jsonRequest = JSON.parse(data.jsonRequest);
                    $rootScope.record = data;
                }).error(function (data, status, headers, config) {
                    $window.alert("Ошибка запроса");
                });
            }
        }
    }]);

jhipsterApp.factory('SERVICE2', ['$rootScope', '$http', '$window',
    function ($rootScope, $http, $window) {
        return {
            sendRequestInsert: function (data) {
                $http.post('app/rest/service2', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (result, status, headers, config) {
                    $rootScope.loading = false;
                    if (result == "false") {
                        $rootScope.rResult = "Ничего не найдено";
                    }
                    else {
                        $rootScope.response = result;
                        var td2 = document.createElement("td");
                        td2.innerHTML = result.id;//data;
                        var td3 = document.createElement("td");
                        td3.innerHTML = "<a href=\"#/service2/view/" + result.id + "\">Посмотреть запрос</a>";
                        var tr = document.createElement("tr");
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        var tableBody = document.getElementById("response_list");
                        tableBody.appendChild(tr);
                    }
                }).error(function (data, status, headers, config) {
                    $rootScope.loading = false;
                    $rootScope.rResult = "Ошибка запроса";
                });
            },

            sendRequestGetAll: function (data) {

                $http.post('app/rest/service/all', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    data.forEach(function (entry) {
                        entry.jsonRequest = JSON.parse(entry.jsonRequest);
                        //console.log(entry.jsonRequest);
                        entry.jsonRequest = entry.jsonRequest.nameYl;
                    });
                    $rootScope.answer = data;
                }).error(function (data, status, headers, config) {
                    //$window.alert("Ошибка запроса");
                });
            },

            sendRequestGet: function (data) {
                $http.post('app/rest/service2/view', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    data.jsonRequest = JSON.parse(data.jsonRequest);
                    $rootScope.record = data;
                }).error(function (data, status, headers, config) {
                    $window.alert("Ошибка запроса");
                });
            }
        }
    }]);

jhipsterApp.factory('SERVICE3', ['$rootScope', '$http', '$window',
    function ($rootScope, $http, $window) {
        return {
            sendRequestInsert: function (data) {
                $http.post('app/rest/service3', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (result, status, headers, config) {
                    $rootScope.loading = false;
                    if (result == "false") {
                        $rootScope.rResult = "Ничего не найдено";
                    }
                    else {
                        $rootScope.response = result;
                        var td2 = document.createElement("td");
                        td2.innerHTML = result.id;//data;
                        var td3 = document.createElement("td");
                        td3.innerHTML = "<a href=\"#/service3/view/" + result.id + "\">Посмотреть запрос</a>";
                        var tr = document.createElement("tr");
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        var tableBody = document.getElementById("response_list");
                        tableBody.appendChild(tr);
                    }
                }).error(function (data, status, headers, config) {
                    $rootScope.loading = false;
                    $rootScope.rResult = "Ошибка запроса";
                });
            },

            sendRequestGetAll: function (data) {

                $http.post('app/rest/service/all', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    data.forEach(function (entry) {
                        entry.jsonRequest = JSON.parse(entry.jsonRequest);
                        //console.log(entry.jsonRequest);
                        entry.jsonRequest = entry.jsonRequest.iin;
                    });
                    $rootScope.answer = data;
                }).error(function (data, status, headers, config) {
                    //$window.alert("Ошибка запроса");
                });
            },

            sendRequestGet: function (data) {
                $http.post('app/rest/service3/view', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    data.jsonRequest = JSON.parse(data.jsonRequest);
                    $rootScope.record = data;
                }).error(function (data, status, headers, config) {
                    $window.alert("Ошибка запроса");
                });
            }
        }
    }]);

jhipsterApp.factory('SERVICE4', ['$rootScope', '$http', '$window',
    function ($rootScope, $http, $window) {
        return {
            sendRequestInsert: function (data) {
                $http.post('app/rest/service4', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (result, status, headers, config) {
                    $rootScope.loading = false;
                    if (result == "false") {
                        $rootScope.rResult = "Ничего не найдено";
                    }
                    else {
                        $rootScope.response = result;
                        var td2 = document.createElement("td");
                        td2.innerHTML = data.replace(new RegExp("request=", 'g'), "");
                        var td3 = document.createElement("td");
                        td3.innerHTML = "<a href=\"#/service4/view/" + result.id + "\">Посмотреть запрос</a>";
                        var tr = document.createElement("tr");
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        var tableBody = document.getElementById("response_list");
                        tableBody.appendChild(tr);
                    }
                }).error(function (data, status, headers, config) {
                    $rootScope.rResult = "Ошибка запроса";
                });
            },

            sendRequestGetAll: function (data) {

                $http.post('app/rest/service/all', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    data.forEach(function (entry) {
                        entry.jsonRequest = JSON.parse(entry.jsonRequest);
                        //console.log(entry.jsonRequest);
                        entry.jsonRequest = entry.jsonRequest.rnn;
                    });
                    $rootScope.answer = data;
                }).error(function (data, status, headers, config) {
                    //$window.alert("Ошибка запроса");
                });
            },

            sendRequestGet: function (data) {
                $http.post('app/rest/service4/view', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    data.jsonRequest = JSON.parse(data.jsonRequest);
                    $rootScope.record = data;
                }).error(function (data, status, headers, config) {
                    $window.alert("Ошибка запроса");
                });
            }
        }
    }]);
/*
 jhipsterApp.factory('SERVICE5', ['$rootScope', '$http',
 function ($rootScope, $http) {
 return {
 sendRequestInsert: function (data) {
 $http.post('app/rest/service5', data, {
 headers: {
 "Content-Type": "application/x-www-form-urlencoded"
 },
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.response = data;
 var td1 = document.createElement("td");
 td1.innerHTML = data.id;
 var td2 = document.createElement("td");
 td2.innerHTML = "<a href=\"#/service5/view/" + data.id + "\">Посмотреть запрос</a>";
 var td3 = document.createElement("td");
 td3.innerHTML = data.serviceType;
 var tr = document.createElement("tr");
 tr.appendChild(td1);
 tr.appendChild(td2);
 tr.appendChild(td3);
 var tableBody = document.getElementById("response_list");
 tableBody.appendChild(tr);
 }).error(function (data, status, headers, config) {
 return "error";
 });
 },

 sendRequestGetAll: function () {
 $http.get('app/rest/service/all').then(function (response) {
 $rootScope.answer = response.data;
 });
 },

 sendRequestGet: function (data) {
 $http.post('app/rest/service5/view', data, {
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.record = data;
 }).error(function (data, status, headers, config) {
 return "error";
 });
 }
 }
 }]);

 jhipsterApp.factory('SERVICE6', ['$rootScope', '$http',
 function ($rootScope, $http) {
 return {
 sendRequestInsert: function (data) {
 $http.post('app/rest/service6', data, {
 headers: {
 "Content-Type": "application/x-www-form-urlencoded"
 },
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.response = data;
 var td1 = document.createElement("td");
 td1.innerHTML = data.id;
 var td2 = document.createElement("td");
 td2.innerHTML = "<a href=\"#/service6/view/" + data.id + "\">Посмотреть запрос</a>";
 var td3 = document.createElement("td");
 td3.innerHTML = data.serviceType;
 var tr = document.createElement("tr");
 tr.appendChild(td1);
 tr.appendChild(td2);
 tr.appendChild(td3);
 var tableBody = document.getElementById("response_list");
 tableBody.appendChild(tr);
 }).error(function (data, status, headers, config) {
 return "error";
 });
 },

 sendRequestGetAll: function () {
 $http.get('app/rest/service/all').then(function (response) {
 $rootScope.answer = response.data;
 });
 },

 sendRequestGet: function (data) {
 $http.post('app/rest/service6/view', data, {
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.record = data;
 }).error(function (data, status, headers, config) {
 return "error";
 });
 }
 }
 }]);

 jhipsterApp.factory('SERVICE7', ['$rootScope', '$http',
 function ($rootScope, $http) {
 return {
 sendRequestInsert: function (data) {
 $http.post('app/rest/service7', data, {
 headers: {
 "Content-Type": "application/x-www-form-urlencoded"
 },
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.response = data;
 var td1 = document.createElement("td");
 td1.innerHTML = data.id;
 var td2 = document.createElement("td");
 td2.innerHTML = "<a href=\"#/service7/view/" + data.id + "\">Посмотреть запрос</a>";
 var td3 = document.createElement("td");
 td3.innerHTML = data.serviceType;
 var tr = document.createElement("tr");
 tr.appendChild(td1);
 tr.appendChild(td2);
 tr.appendChild(td3);
 var tableBody = document.getElementById("response_list");
 tableBody.appendChild(tr);
 }).error(function (data, status, headers, config) {
 return "error";
 });
 },

 sendRequestGetAll: function () {
 $http.get('app/rest/service/all').then(function (response) {
 $rootScope.answer = response.data;
 });
 },

 sendRequestGet: function (data) {
 $http.post('app/rest/service7/view', data, {
 ignoreAuthModule: 'ignoreAuthModule'
 }).success(function (data, status, headers, config) {
 $rootScope.record = data;
 }).error(function (data, status, headers, config) {
 return "error";
 });
 }
 }
 }]);
 */
jhipsterApp.factory('SERVICE8', ['$rootScope', '$http', '$window',
    function ($rootScope, $http, $window) {
        return {
            sendRequestInsert: function (data) {
                $http.post('app/rest/service8', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (result, status, headers, config) {
                    $rootScope.loading = false;
                    if (result == "false") {
                        $rootScope.rResult = "Ничего не найдено";
                    }
                    else {
                        $rootScope.response = result;
                        var td2 = document.createElement("td");
                        td2.innerHTML = data.replace(new RegExp("request=", 'g'), "");
                        var td3 = document.createElement("td");
                        td3.innerHTML = "<a href=\"#/service8/view/" + result.id + "\">Посмотреть запрос</a>";
                        var tr = document.createElement("tr");
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        var tableBody = document.getElementById("response_list");
                        tableBody.appendChild(tr);
                    }
                }).error(function (data, status, headers, config) {
                    $rootScope.rResult = "Ошибка запроса";
                });
            },

            sendRequestGetAll: function (data) {

                $http.post('app/rest/service/all', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    //console.log(data);
                    data.forEach(function (entry) {
                        entry.jsonRequest = JSON.parse(entry.jsonRequest);
                        //console.log(entry.jsonRequest);
                        entry.jsonRequest = entry.jsonRequest.numberWritOfExecution;
                    });
                    $rootScope.answer = data;
                }).error(function (data, status, headers, config) {
                    //$window.alert("Ошибка запроса");
                });
            },

            sendRequestGet: function (data) {
                $http.post('app/rest/service8/view', data, {
                    ignoreAuthModule: 'ignoreAuthModule'
                }).success(function (data, status, headers, config) {
                    data.jsonRequest = JSON.parse(data.jsonRequest);
                    $rootScope.record = data;
                }).error(function (data, status, headers, config) {
                    $window.alert("Ошибка запроса");
                });
            }
        }
    }]);
