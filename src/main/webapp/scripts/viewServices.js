function viewService1Controller($scope, $routeParams, SERVICE1) {
    $scope.find = function() {
        SERVICE1.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService2Controller($scope, $routeParams, SERVICE2) {
    $scope.find = function() {
        SERVICE2.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService3Controller($scope, $routeParams, SERVICE3) {
    $scope.find = function() {
        SERVICE3.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService4Controller($scope, $routeParams, SERVICE4) {
    $scope.find = function() {
        SERVICE4.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService5Controller($scope, $routeParams, SERVICE5) {
    $scope.find = function() {
        SERVICE5.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService6Controller($scope, $routeParams, SERVICE6) {
    $scope.find = function() {
        SERVICE6.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService7Controller($scope, $routeParams, SERVICE7) {
    $scope.find = function() {
        SERVICE7.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function viewService8Controller($scope, $routeParams, SERVICE8) {
    $scope.find = function() {
        SERVICE8.sendRequestGet($routeParams.id);
    };
    $scope.find();
}

function stringToXML($scope, $rootScope, $window, xmlParser) {
    $scope.xml = function (param) {
        var domElement = (param == 1) ? xmlParser.parse($rootScope.record.xmlRequest) : xmlParser.parse($rootScope.record.xmlResponse);
        $scope.answer = (new XMLSerializer()).serializeToString(domElement);
        //console.log(domElement);
        /*var xmlDoc = jQuery.parseXML($rootScope.record.xmlRequest);
         if (xmlDoc) {
         $window.alert(xmlDoc);
         }*/
        /*if (window.ActiveXObject){
         var doc=new ActiveXObject('Microsoft.XMLDOM');
         doc.async='false';
         doc.loadXML($rootScope.record.xmlRequest);
         } else {
         var parser=new DOMParser();
         var doc=parser.parseFromString($rootScope.record.xmlRequest,'text/xml');
         }*/
        //$window.alert(doc);
        //return doc;
        $("#dialog").dialog({
            width: 600,
            modal: true
        });
        //$window.alert($rootScope.record.xmlRequest);
    };
}